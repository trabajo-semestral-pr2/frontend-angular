// src/app/inscripcion-alumno/inscripcion-alumno.component.spec.ts
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { InscripcionAlumnoComponent } from './inscripcion-alumno.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('InscripcionAlumnoComponent', () => {
  let component: InscripcionAlumnoComponent;
  let fixture: ComponentFixture<InscripcionAlumnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InscripcionAlumnoComponent],
      imports: [FormsModule, HttpClientTestingModule],
    
    })
    .compileComponents();

    fixture = TestBed.createComponent(InscripcionAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a valid form', () => {
    const alumnoForm = component.alumno;
    alumnoForm.cedula = '12345678';
    alumnoForm.nombre = 'Juan';
    alumnoForm.apellido = 'Perez';
    alumnoForm.fecha_nac = '2000-01-01';

    const padreForm = component.padre_encargado;
    padreForm.cedula = '87654321';
    padreForm.nombre = 'Pedro';
    padreForm.apellido = 'Perez';
    padreForm.direccion = 'Av. Siempre Viva';
    padreForm.telefono = '555-1234';
    padreForm.correo = 'pedro@example.com';
    padreForm.emergencia = 'S';
    padreForm.retiro = 'N';

    const facturacionForm = component.facturacion;
    facturacionForm.ruc = '123456789';
    facturacionForm.razon_social = 'Empresa SA';

    const inscripcionForm = component.inscripcion;
    inscripcionForm.fecha_inscripcion = '2023-01-01';
   
    inscripcionForm.estado = 'Pendiente de pago';

    expect(inscripcionForm.fecha_inscripcion).toBeTruthy();
    expect(inscripcionForm.id_curso).toBeTruthy();
  });
});
