// src/app/inscripcion-alumno/inscripcion-alumno.component.ts
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { padre_encargado, alumno, facturacion, inscripcion } from '../models/model-Alumno';
import { AlumnosService } from '../services/alumnos.service';
@Component({
  selector: 'app-inscripcion-alumno',
  templateUrl: './inscripcion-alumno.component.html',
  styleUrls: ['./inscripcion-alumno.component.css']
})
export class InscripcionAlumnoComponent implements OnInit{

  modalPadres     : any;
  listCurso       : any[] = [];
  listPadres      : padre_encargado[] = [];
  padre_encargado : padre_encargado   = {};
  alumno          : alumno            = {};
  facturacion     : facturacion       = {};
  inscripcion     : inscripcion       = {};
  constructor(private alumnoService:AlumnosService) {}
  ngOnInit(): void {
    this.obtenerCurso();
  }
  openModal() {
    document.getElementById('modalPadres')!.style.display = 'block';

  }

  closeModal() {
    document.getElementById('modalPadres')!.style.display = 'none';
  }

  obtenerCurso(){
    this.alumnoService.obtenerCursos().subscribe(data=>{
      if(data){
        this.listCurso=data;
      }
    })
  }

  insertPadreEncargado(){
    const padreAux = Object.assign({},this.padre_encargado)
    this.listPadres.push(padreAux);
    this.closeModal();

  }


  mostrarFrmTuto(){}

   onSubmit() {
    const formData = new FormData();
    formData.append('alumno', JSON.stringify(this.alumno));
    formData.append('padre_encargado', JSON.stringify(this.padre_encargado));
    formData.append('facturacion', JSON.stringify(this.facturacion));
    formData.append('inscripcion', JSON.stringify(this.inscripcion));
    if (this.inscripcion.contrato) {
      formData.append('contrato', this.inscripcion.contrato);
    }




  }

  prevIncripcion(){
    this.inscribirAlumno().then((data:any)=>{
        if(data){
          console.info(data)
        }
     //
    })
  }

  async inscribirAlumno(): Promise<any>{
    let filtro ={
        alumno: {
            Cedula: this.alumno.cedula,
            Nombre: this.alumno.nombre,
            Apellido: this.alumno.apellido,
            Fecha_Nac: this.alumno.fecha_nac,
            ID_Facturacion: {
                Ruc: this.facturacion.ruc,
                Razon_Social: this.facturacion.razon_social
            }
        },
        ID_Curso: this.inscripcion.id_curso,
        Fecha_inscripcion: this.inscripcion.fecha_inscripcion,
        Firmado: this.inscripcion.firmado,
        Estado: this.inscripcion.estado
    }
    return await this.alumnoService.inscribirAlumno(filtro).toPromise();


  }
}
