export interface alumno  {
  cedula?    : string;
  nombre?    : string;
  apellido?  : string;
  fecha_nac? : string;
};

export interface padre_encargado  {
  cedula?      : string;
  nombre?      : string;
  apellido?    : string;
  direccion?   : string;
  telefono?    : string;
  correo?      : string;
  emergencia?  : string;
  retiro?      : string;
};

export interface facturacion  {
  ruc?           : string,
  razon_social?  : string,
};

export interface inscripcion  {
  fecha_inscripcion? : string;
  contrato?          : any;
  firmado?           : string;
  id_curso?          : Number;
  estado?            : string;
};
