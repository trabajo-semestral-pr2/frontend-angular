import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InscripcionAlumnoComponent } from './inscripcion-alumno/inscripcion-alumno.component';
import { GestionMateriaComponent } from './gestion-materia/gestion-materia.component';
import { GestionCursoComponent } from './gestion-curso/gestion-curso.component';


const routes: Routes = [
    {
      path: 'inscripcion',
      component: InscripcionAlumnoComponent,
    },
    {
      path: 'materia',
      component: GestionMateriaComponent,
    },
    {
    path: 'curso',
    component: GestionCursoComponent,
    },
];
  

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true }
    )],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  