import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'  // Esto proporciona el servicio en el módulo raíz
})
export class AlumnosService {  // Cambiado el nombre a AlumnosService por convención
  constructor(private http: HttpClient) {}

  inscribirAlumno(param : any  ): Observable<any> {
    return this.http.post<any>(`http://localhost:8000/api/inscripciones/`, param);
  }

  obtenerCursos(): Observable<any> {
    return this.http.get<any>(`http://localhost:8000/api/cursos/`);
  }
}
