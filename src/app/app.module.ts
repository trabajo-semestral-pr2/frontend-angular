import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { InscripcionAlumnoComponent } from './inscripcion-alumno/inscripcion-alumno.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './_body/header/header.component';
import { AppBodyComponent } from './_body/app-body/app-body.component';
import { GestionMateriaComponent } from './gestion-materia/gestion-materia.component';
import { GestionCursoComponent } from './gestion-curso/gestion-curso.component';
import { AlumnosService } from './services/alumnos.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    InscripcionAlumnoComponent,
    HeaderComponent,
    AppBodyComponent,
    GestionMateriaComponent,
    GestionCursoComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AlumnosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
