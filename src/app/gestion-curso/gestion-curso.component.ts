import { Component, OnInit } from '@angular/core';
import { AlumnosService } from '../services/alumnos.service';

interface Curso {
  Nivel: string;
  Ciclo: string;
  Especializacion: string;
  GradoCurso: string;
  Turno: string;
  Matricula: number | string;
  Cuota: number | string;
}

@Component({
  selector: 'app-gestion-curso',
  templateUrl: './gestion-curso.component.html',
  styleUrls: ['./gestion-curso.component.css']
})
export class GestionCursoComponent implements OnInit{
  cursos: Curso[] = [];

  constructor(private alumnosService:AlumnosService){

  }

  ngOnInit(): void {
    this.obtenerCursos();
  }

  obtenerCursos(){
    this.alumnosService.obtenerCursos().subscribe(data=>{
      if(data){
        this.cursos=data
      }
    })
  }

}
